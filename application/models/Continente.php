<?php

    //constructor
    class Continente extends CI_Model{
      //funcion constructor
        public function __construct(){
            parent:: __construct();
        }


        public function insertar($datos){
            return $this->db->insert('continente',$datos);
        }

        public function actualizar($id_con,$datos){
          $this->db->where("id_con",$id_con);
            return $this->db->update("continente",$datos);
        }

        public function consultarPorId($id_con){
          $this->db->where("id_con",$id_con);
          $this->db->join("continente","continente.id_con=continente.fk_id_con");
            $continente=$this->db->get('continente');
            if ($continente->num_rows()>0) {
                return $continente->row();
            } else {
                return false;
            }

        }

        public function consultarTodos(){
          $this->db->join("continente","continente.id_con=continente.fk_id_con");
            $listadoContinentes=$this->db->get('continente');
            if ($listadoContinentes->num_rows()>0) {
                return $listadoContinentes;
            } else {
                return false;
            }
        }

        public function eliminar($id_con){
        $this->db->where("id_con",$id_con);
        return $this->db->delete("continente");
    }


    }

 ?>
