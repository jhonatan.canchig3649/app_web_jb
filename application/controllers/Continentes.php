<?php
    class Continentes extends CI_Controller{
        public function __construct(){
            parent::__construct();
            $this->load->model("continente");
            $this->load->model("pais");
        }

        public function index(){
            $data["listadoContinentes"]=$this->continentes->consultarTodos();
            $this->load->view('header');
            $this->load->view('continentes/index',$data);
            $this->load->view('footer');
        }
        //funcion Nuevo
        public function nuevo(){
          //modelo
            $data["listadoPaises"]=$this->pais->consultarTodos();
            $this->load->view('header');
            $this->load->view('continentes/nuevo',$data);
            $this->load->view('footer');
        }

        //funcion editar
        public function editar($id_con){
          //modelo
            $data["listadoPaises"]=$this->pais->consultarTodos();
            $data["continente"]=$this->continente->consultarPorId($id_con);
            $this->load->view('header');
            $this->load->view('continentes/editar',$data);
            $this->load->view('footer');
        }

        public function procesarActualizacion(){
          $id_con=$this->input->post("id_con");
          $datosContinenteEditado=array(
              "nombre_con"=>$this->input->post("nombre_con"),
              "numero_de_paises_con"=>$this->input->post("numero_de_paises_con"),
              "hemisferio_con"=>$this->input->post("hemisferio_con"),
            );
            if ($this->continente->actualizar($id_con,$datosContinenteEditado)){
              $this->session->set_flashdata("confirmacion","continente insertado exitosamente.");

            } else {
                $this->session->set_flashdata("Error","Error al procesar, intente nuevamente");
            }
            redirect("continentes/index");
        }

        //registro de usuarios
        public function guardarContinente(){
          $datosNuevoContinente=array(
            "nombre_con"=>$this->input->post("nombre_con"),
            "numero_de_paises_con"=>$this->input->post("numero_de_paises_con"),
            "hemisferio_con"=>$this->input->post("hemisferio_con")
          );

          if ($this->continentes->insertar($datosNuevoContinente)) {
            //nombre variable contenido variable
                $this->session->set_flashdata("confirmacion","continente insertado exitosamente.");
          } else {
                $this->session->set_flashdata("Error","Error al procesar, intente nuevamente");
          }
          redirect("continentes/index");
        }

        function procesarEliminacion($id_con){
                if ($this->continente->eliminar($id_con)) {
                  $this->session->set_flashdata("confirmacion","continente eliminado exitosamente.");
                } else {
                  $this->session->set_flashdata("Error","Error al procesar, intente nuevamente");
                }
                redirect("continentes/index");
        }



    }//cierre funcion
 ?>
