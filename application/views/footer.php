</div>
</div>
</div>
<!-- content-wrapper ends -->
    <!-- partial:partials/_footer.html -->
    <footer class="footer">
      <div class="d-sm-flex justify-content-center justify-content-sm-between">
        <span class="text-muted text-center text-sm-left d-block d-sm-inline-block">Copyright © 2021.  Premium <a href="https://www.bootstrapdash.com/" target="_blank">Bootstrap admin template</a> from BootstrapDash. All rights reserved.</span>
        <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Hand-crafted & made with <i class="ti-heart text-danger ml-1"></i></span>
      </div>
      <div class="d-sm-flex justify-content-center justify-content-sm-between">
        <span class="text-muted text-center text-sm-left d-block d-sm-inline-block">Distributed by <a href="https://www.themewagon.com/" target="_blank">Themewagon</a></span>
      </div>
    </footer>
    <!-- partial -->
  </div>
  <!-- main-panel ends -->
</div>
<!-- page-body-wrapper ends -->
</div>
<!-- container-scroller -->

<!-- plugins:js -->
<script src="<?php echo base_url();?>/assets/vendors/js/vendor.bundle.base.js"></script>
<!-- endinject -->
<!-- Plugin js for this page -->
<script src="<?php echo base_url();?>/assets/vendors/chart.js/Chart.min.js"></script>
<script src="<?php echo base_url();?>/assets/vendors/datatables.net/jquery.dataTables.js"></script>
<script src="<?php echo base_url();?>/assets/vendors/datatables.net-bs4/dataTables.bootstrap4.js"></script>
<script src="<?php echo base_url();?>/assets/js/dataTables.select.min.js"></script>

<!-- End plugin js for this page -->
<!-- inject:js -->
<script src="<?php echo base_url();?>/assets/js/off-canvas.js"></script>
<script src="<?php echo base_url();?>/assets/js/hoverable-collapse.js"></script>
<script src="<?php echo base_url();?>/assets/js/template.js"></script>
<script src="<?php echo base_url();?>/assets/js/settings.js"></script>
<script src="<?php echo base_url();?>/assets/js/todolist.js"></script>
<!-- endinject -->
<!-- Custom js for this page-->
<script src="<?php echo base_url();?>/assets/js/dashboard.js"></script>
<script src="<?php echo base_url();?>/assets/js/Chart.roundedBarCharts.js"></script>
<!-- End custom js for this page-->


<?php if ($this->session->flashdata("confirmacion")): ?>
<!--mensajes de alerta-->
<script type="text/javascript">
    iziToast.success({
        title: 'confirmacion',
        message: '<?php echo $this->session->flashdata("confirmacion") ?>',
        position: 'topRight'
    })
</script>
<?php endif; ?>

<?php if ($this->session->flashdata("error")): ?>
<!--mensajes de alerta-->
<script type="text/javascript">
    iziToast.danger({
        title: 'Advertencia',
        message: '<?php echo $this->session->flashdata("error") ?>',
        position: 'topRight'
    })
</script>
<?php endif; ?>

<style media="screen">
.error{
  color:red;
  font-size: 16px;
}
input.error, select.error{
  border:2px solid red;
}
</style>


</body>

</html>
