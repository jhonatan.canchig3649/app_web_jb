<div class="col-md-12">
    <div class="row">
      <div class="col-md-2"></div>
      <div class="col-md-8">
          <br>
            <center>
              <br>
              <h2> <b>Nuevo continente</b> </h2>
              <hr>
              <br>
            </center>
            <form action="<?php echo site_url(); ?>/continentes/procesarActualizacion" method="post" id="frm_nuevo_continentes">
              <label for="">continente</label>
              <select class="form-control" name="fk_id_con" id="fk_id_con">
                  <option value="">--Seleccione un continente--</option>
                  <?php if ($listadoContinentes): ?>
                        <?php foreach ($listadoContinentes->result() as $conTemporal): ?>
                              <option value="<?php echo $conTemporal->id_con ?>">
                                  <?php echo $conTemporal->nombre_con ?>
                              </option>
                        <?php endforeach; ?>
                  <?php endif; ?>
              </select>
              <br>
              <label for="">Nombre</label><br>
              <input type="text" name="nombre_con" id="nombre_con" value="" placeholder="Ingrese el nombre"
              class="form-control"><br>
              <label for="">Numero de Paises</label><br>
              <input type="number" name="numero_de_paises_con" id="numero_de_paises_con" value="" placeholder="Ingrese el paises"
              class="form-control"><br>
              <label for="">Hemisferios</label><br>
              <select class="form-control" name="hemisferio_con" id="hemisferio_con">
                  <option value="">Seleccione...</option>
                  <option value="Norte">Norte</option>
                  <option value="Sur">Sur</option>
              </select><br>
              <button type="submit" class="btn btn-info" name="button"> <i class="fa-solid fa-floppy-disk"></i> Guardar</button>
            &nbsp;&nbsp;&nbsp
            <a href="<?php echo site_url(); ?>/continentes/index" class="btn btn-danger"> <i class="fa fa-times"></i> Cancelar</a>
            <br><br>
            </form>
      </div>
      <div class="col-md-2"></div>
    </div>

</div>

<script type="text/javascript">
    $("#frm_nuevo_continentes").validate({
      rules:{
        fk_id_con:{
          required:true
        },
        nombre_con:{
          required:true,
          letras:true
        },
        numero_de_paises_con:{
          required:true,
          minlength:1,
          maxlength:3,
          digits:true
        },
        hemisferio_con:{
          required:true
        }
      },


      messages:{
        fk_id_con:{
          required:"Seleccione uno"
        },
        nombre_con:{
          required:"Ingrese un nombre",
          letras:"Solo se acepta letras"
        },
        numero_de_paises_con:{
          required:"Ingrese un mumero de pises",
          minlength:"debe tener minimo un numero",
          maxlength:"debe tener maximo tres numeros",
          digits:"Solo se aceptan numeros"
        },
        hemisferio_con:{
          required:"Seleccione uno por favor"
        }
        }
    });
</script>
