<div class="col-md-12">
          <br>
          <center>
              <h2><b>Listado de continentes</b></h2>
              <br>
              <a href="<?php echo site_url(); ?>/continentes/nuevo" class="btn btn-info"> <i class="fa fa-plus"></i> Agregar Nuevo </a>
          </center>
          <br>

              <?php if ($listadoContinentes): ?>
                    <table class="table table-bordered table-striped table-hover" id="tbl-continentes">
                        <thead>
                          <tr>
                            <th class="text-center">ID</th>
                            <th class="text-center">NOMBRE</th>
                            <th class="text-center">NUMERO DE PAISES</th>
                            <th class="text-center">HEMISFERIOS</th>
                            <th class="text-center">OPCIONES</th>
                          </tr>
                        </thead>

                        <tbody>
                          <?php foreach ($listadoContinentes->result() as $filaTemporal): ?>
                              <tr>
                                  <td class="text-center">
                                      <?php echo $filaTemporal->id_con; ?>
                                  </td>
                                  <td class="text-center">
                                  </td>
                                  <td class="text-center">
                                      <?php echo $filaTemporal->nombre_con; ?>
                                  </td>
                                  <td class="text-center">
                                      <?php echo $filaTemporal->numero_de_paises_con; ?>
                                  </td>
                                  <td class="text-center">
                                      <?php echo $filaTemporal->nombre_pais ?>
                                  </td>
                                  <td class="text-center">
                                      <a href="<?php echo site_url() ?>/continentes/editar/<?php echo $filaTemporal->id_con ?>" class="btn btn-warning"> <i class="fa fa-pen"></i> </a>
                                      <a class="btn btn-danger" href="javascript:void(0)" onclick="confirmarEliminacion('<?php echo $filaTemporal->id_con; ?>')" ><strong style="color:white;"><i class="fa fa-trash"></i></strong></a>
                                  </td>
                              </tr>
                          <?php endforeach; ?>
                        </tbody>
                    </table>
              <?php else: ?>
                    <div class="alert alert-danger">
                        <h3>No se encontraron continentes resgitrados</h3>
                    </div>
              <?php endif; ?>
</div>

<script type="text/javascript">
    function confirmarEliminacion(id_con){
          iziToast.question({
              timeout: 20000,
              close: false,
              overlay: true,
              displayMode: 'once',
              id: 'question',
              zindex: 999,
              title: 'CONFIRMACIÓN',
              message: '¿Esta seguro de eliminar el continente de forma pernante?',
              position: 'center',
              buttons: [
                  ['<button><b>SI</b></button>', function (instance, toast) {

                      instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');
                      window.location.href=
                      "<?php echo site_url(); ?>/continentes/procesarEliminacion/"+id_con;

                  }, true],
                  ['<button>NO</button>', function (instance, toast) {

                      instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');

                  }],
              ]
          });
    }
</script>

<script type="text/javascript">
    $("#tbl-continentes").DataTable();
</script>
